package com.example.peneil.examapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
            Log.v("ACTION_SCREEN_OFF", "In Method:  ACTION_SCREEN_OFF");
        }
        else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
            Log.v("ACTION_SCREEN_ON", "In Method:  ACTION_SCREEN_ON");
        }
        else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            Log.v("ACTION_USER_PRESE", "In Method:  ACTION_USER_PRESENT");
            MyService ms = new MyService();
            ms.onCreate();
        }
    }
}
