package com.example.peneil.examapp;

import android.app.Service;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyService extends Service {
    //Settings > Apps > [Your App] > Permissions and enable the Camera setting.

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CapturePhoto();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private void CapturePhoto() {
        Log.d("CapturePhoto","Preparing to take photo");
        Camera camera = null;

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        int frontCamera = 1;
        //int backCamera=0;

        Camera.getCameraInfo(frontCamera, cameraInfo);
        Log.i("cameraInfo", String.valueOf(Camera.getNumberOfCameras()));

        try {
            camera = Camera.open(frontCamera);
        } catch (RuntimeException e) {
            Log.d("Camera Error","Camera not available: " + e);
            camera = null;
            //e.printStackTrace();
        }
        try {
            if (null == camera) {
                Log.d("Camera Instance Not Get","Could not get camera instance");
            } else {
                Log.d("Got The Camera","Got the camera, creating the dummy surface texture");
                try {
                    Log.d("No Error","no error");
                    camera.setPreviewTexture(new SurfaceTexture(0));
                    camera.startPreview();
                } catch (Exception e) {
                    Log.d("Exception Error","Could not set the surface preview texture");
                    e.printStackTrace();
                }
                camera.takePicture(null, null, new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        File pictureFileDir = new File(Environment.getExternalStorageDirectory()
                                + "/myDir/");
                        if (!pictureFileDir.exists()) {
                            pictureFileDir.mkdir();
                        }
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
                        String date = dateFormat.format(new Date());
                        String photoFile = "ExamAppPic_" + "_" + date + ".jpg";
                        String filename = pictureFileDir.getPath() + File.separator + photoFile;
                        File mainPicture = new File(filename);
                        try {
                            FileOutputStream fos = new FileOutputStream(mainPicture);
                            fos.write(data);
                            fos.close();
                        } catch (Exception error) {
                            Log.d("FileOutputStream error","Image could not be saved"+error);
                        }
                        camera.release();
                    }
                });
            }
        } catch (Exception e) {
            camera.release();
        }
    }
}
